# Geojson Indonesia
Geojson collection of Indonesia that is compatible with [Highcharts Maps](https://www.highcharts.com/demo/maps). 

## Available Data
- [Separate geojson data per province](https://gitlab.com/otls/geojson-indonesia/-/tree/main/provinsi)

## Highcharts Maps Integration
Please check `highchart-example` folder

## Todo
- separate geojson data per regency 
- separate geojson data per district

## Contributing
Thank you for considering contributing, very much appreciated. 

## Data Source
- [GADM maps and data](https://gadm.org/index.html)
